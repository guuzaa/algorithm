package list

import (
	"reflect"
	"testing"
)

func TestFindPeakElement(t *testing.T) {
	tests := []struct {
		input    []int
		expected int
	}{
		{[]int{1, 2, 3, 1}, 2},
		{[]int{1, 2, 1, 1}, 1},
		{[]int{1, 10, 9, 8, 7, 1, 1}, 1},
	}

	for _, tt := range tests {
		if ret := FindPeakElement(tt.input); ret != tt.expected {
			t.Errorf("1 Error with %v", tt.input)
		}

		if ret := FindPeakElement2(tt.input); ret != tt.expected {
			t.Errorf("2 Error with %v", tt.input)
		}
	}

}

func TestPickPeaks(t *testing.T) {
	tests := []struct {
		input    []int
		expected PosPeaks
	}{
		{[]int{2, 1, 3, 1, 2, 2, 2, 2}, PosPeaks{Pos: []int{2}, Peaks: []int{3}}},
		{[]int{2, 1, 3, 1, 2, 2, 2, 2, 1}, PosPeaks{Pos: []int{2, 4}, Peaks: []int{3, 2}}},
		{[]int{1, -5, 7, 0, 0, 10, 2, 5, 5, 13}, PosPeaks{Pos: []int{2, 5}, Peaks: []int{7, 10}}},
		{[]int{13, 9, -2, -5, 8, 8, 14, -2, -3}, PosPeaks{Pos: []int{6}, Peaks: []int{14}}},
		{[]int{3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 3}, PosPeaks{Pos: []int{3, 7}, Peaks: []int{6, 3}}},
		{[]int{3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 2, 2, 1}, PosPeaks{Pos: []int{3, 7, 10}, Peaks: []int{6, 3, 2}}},
	}

	for _, tt := range tests {
		if ret := PickPeaks(tt.input); !reflect.DeepEqual(ret, tt.expected) {
			t.Errorf("Error with %v", tt.input)
		}
	}
}
