package list

import "sort"

// Problem Definition: https://www.nowcoder.com/practice/eac1c953170243338f941959146ac4bf
func MaxIncreasingSubseq(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	sort.Ints(nums)

	maxLen := 1
	ll := 1
	for i := 1; i < len(nums); i++ {
		if nums[i] == nums[i-1] {
			continue
		}

		if nums[i] == nums[i-1]+1 {
			ll++
		} else {
			ll = 1
		}

		maxLen = max(maxLen, ll)
	}

	return maxLen
}

func max(a, b int) int {
	if a >= b {
		return a
	}
	return b
}
