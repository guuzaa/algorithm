package list

import "testing"

func TestMedianFinder(t *testing.T) {

	finder := Constructor()
	finder.AddNum(1)
	finder.AddNum(2)
	if ret := finder.FindMedian(); ret != 1.5 {
		t.Errorf("Error with FindMedian()")
	}

	finder.AddNum(4)
	if ret := finder.FindMedian(); ret != 2 {
		t.Errorf("Error with FindMedian()")
	}

	finder.AddNum(10)
	if ret := finder.FindMedian(); ret != 3 {
		t.Errorf("Error with FindMedian()")
	}
}
