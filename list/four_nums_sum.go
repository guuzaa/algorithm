package list

// Problem Definition: https://leetcode.cn/problems/4sum-ii/
func fourSumCount(nums1, nums2, nums3, nums4 []int) int {
	hash := make(map[int]int)
	for i := 0; i < len(nums1); i++ {
		for j := 0; j < len(nums2); j++ {
			hash[nums1[i]+nums2[j]]++
		}
	}

	var cnt int
	for i := 0; i < len(nums3); i++ {
		for j := 0; j < len(nums4); j++ {
			ret := nums3[i] + nums4[j]
			if val, ok := hash[-ret]; ok {
				cnt += val
			}
		}
	}
	return cnt
}
