package list

import (
	"reflect"
	"testing"
)

func TestPermutation(t *testing.T) {
	tests := []struct {
		input  []int
		output [][]int
	}{
		{[]int{}, [][]int{{}}},
		{[]int{0, 1}, [][]int{{0, 1}, {1, 0}}},
		{[]int{1, 2, 3}, [][]int{{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}}},
	}

	for _, tt := range tests {
		if ret := permute(tt.input); !reflect.DeepEqual(ret, tt.output) {
			t.Errorf("Error with %v, result: %v", tt.input, ret)
		}
	}

}
