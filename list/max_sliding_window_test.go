package list

import (
	"reflect"
	"testing"
)

func TestMaxSlidingWindow(t *testing.T) {
	tests := []struct {
		input  []int
		k      int
		output []int
	}{
		{[]int{2, 3, 4, 2, 6, 2, 5, 1}, 3, []int{4, 4, 6, 6, 6, 5}},
		{[]int{9, 10, 9, -7, -3, 8, 2, -6}, 5, []int{10, 10, 9, 8}},
		{[]int{1, 2, 3, 4}, 3, []int{3, 4}},
	}

	for _, tt := range tests {
		if ret := maxSlidingWindow(tt.input, tt.k); !reflect.DeepEqual(ret, tt.output) {
			t.Errorf("Error with %v", tt.input)
		}
	}
}
