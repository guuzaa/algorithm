package list

import "testing"

func TestMaxSquare(t *testing.T) {
	tests := []struct {
		matrix [][]int
		want   int
	}{
		{[][]int{
			{1, 0, 1, 1, 1, 1, 1, 0, 0, 0},
			{1, 0, 1, 1, 1, 0, 0, 0, 0, 0},
			{1, 0, 1, 0, 1, 1, 1, 0, 0, 0},
			{1, 1, 0, 1, 1, 1, 1, 0, 1, 0},
			{1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
			{1, 0, 1, 1, 1, 1, 1, 1, 1, 0},
			{1, 0, 1, 1, 1, 1, 1, 1, 1, 0},
			{1, 0, 1, 1, 1, 1, 1, 1, 1, 0},
			{1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
			{1, 0, 1, 1, 0, 1, 1, 0, 0, 0}}, 25},
		{[][]int{}, 0},
		{[][]int{
			{1, 0, 1, 0, 0},
			{1, 0, 1, 1, 1},
			{1, 1, 1, 1, 1},
			{1, 0, 0, 1, 0}}, 4},
	}

	for i, tt := range tests {
		if ret := maxSquare(tt.matrix); ret != tt.want {
			t.Errorf("#%dError result: %v, want %v", i, ret, tt.want)
		}
	}
}
