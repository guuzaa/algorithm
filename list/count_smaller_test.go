package list

import (
	"reflect"
	"testing"
)

func TestCountSmaller(t *testing.T) {
	tests := []struct {
		nums     []int
		expected []int
	}{
		{[]int{5, 2, 6, 1}, []int{2, 1, 1, 0}},
		{[]int{1, 9, 7, 8, 5}, []int{0, 3, 1, 1, 0}},
		{[]int{-1}, []int{0}},
		{[]int{-1, -1}, []int{0, 0}},
	}

	for _, tt := range tests {
		ret := countSmaller(tt.nums)
		if !reflect.DeepEqual(ret, tt.expected) {
			t.Errorf("There is something wrong with %v", tt.nums)
		}
	}
}
