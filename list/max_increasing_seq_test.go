package list

import "testing"

func TestMaxIncreasingSubSeq(t *testing.T) {
	tests := []struct {
		input  []int
		output int
	}{
		{[]int{100, 4, 200, 1, 3, 2}, 4},
		{[]int{1, 1, 1}, 1},
		{[]int{1}, 1},
		{[]int{}, 0},
		{[]int{1, 2, 1, 3, 2, 3}, 3},
	}

	for _, tt := range tests {
		if ret := MaxIncreasingSubseq(tt.input); tt.output != ret {
			t.Errorf("Error with %v; result: %v, expected: %v", tt.input, ret, tt.output)
		}
	}
}
