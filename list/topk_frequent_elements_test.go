package list

import (
	"reflect"
	"testing"
)

func TestTopKFrequent(t *testing.T) {
	tests := []struct {
		input, output []int
		k             int
	}{
		{[]int{1, 1, 1, 2, 3, 2}, []int{1, 2}, 2},
		{[]int{-1, -1}, []int{-1}, 1},
		{[]int{1}, []int{1}, 1},
	}

	for _, tt := range tests {
		if ret := topKFrequent(tt.input, tt.k); !reflect.DeepEqual(ret, tt.output) {
			t.Errorf("Error with %v", tt.input)
		}
	}
}
