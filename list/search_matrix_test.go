package list

import "testing"

func TestSearchMatrix(t *testing.T) {
	tests := []struct {
		matrix [][]int
		target int
		output bool
	}{
		{[][]int{{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}}, 5, true},
		{[][]int{{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}}, 52, false},
		{[][]int{{1}}, 1, true},
		{[][]int{{1}}, 10, false},
		{[][]int{{1, 10, 20}}, 10, true},
	}

	for _, tt := range tests {
		if ret := SearchMatrix(tt.matrix, tt.target); ret != tt.output {
			t.Errorf("Error with %v", tt.matrix)
		}
	}
}

func TestKthSmallest(t *testing.T) {
	tests := []struct {
		matrix    [][]int
		k, target int
	}{
		{[][]int{{-5}}, 1, -5},
		{[][]int{{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}}, 5, 5},
		{[][]int{{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}}, 1, 1},
		{[][]int{{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}}, 25, 30},
	}

	for _, tt := range tests {
		if ret := kthSmallest(tt.matrix, tt.k); ret != tt.target {
			t.Errorf("Error with %v", tt.matrix)
		}
	}
}
