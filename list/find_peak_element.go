package list

// Problem Definition: https://leetcode.cn/problems/find-peak-element/
func FindPeakElement(nums []int) int {
	l, r := 0, len(nums)-1
	for l < r {
		mid := l + (r-l)/2
		if nums[mid] < nums[mid+1] {
			l = mid + 1
		} else {
			r = mid
		}
	}

	return l
}

func FindPeakElement2(nums []int) int {
	l := 0
	for l < len(nums)-1 {
		if nums[l] > nums[l+1] {
			return l
		}
		l++
	}

	return l - 1
}

// Problem Definition: https://www.codewars.com/kata/5279f6fe5ab7f447890006a7/train/go/631ebd3df5bd160058f787c2
type PosPeaks struct {
	Pos, Peaks []int
}

func PickPeaks(arr []int) PosPeaks {
	var pos, peaks []int
	candicate := 0
	for i := 1; i < len(arr); i++ {
		if arr[i-1] < arr[i] {
			candicate = i
		} else if candicate != 0 && arr[i-1] > arr[i] {
			pos = append(pos, candicate)
			peaks = append(peaks, arr[candicate])
			candicate = 0
		}
	}

	return PosPeaks{Pos: pos, Peaks: peaks}
}
