package list

import "testing"

func TestFourSumCount(t *testing.T) {
	tests := []struct {
		n1, n2, n3, n4 []int
		expected       int
	}{
		{[]int{1, 2}, []int{-2, -1}, []int{-1, 2}, []int{0, 2}, 2},
		{[]int{-1, -1}, []int{-1, 1}, []int{-1, 1}, []int{1, -1}, 6},
	}

	for _, tt := range tests {
		if ret := fourSumCount(tt.n1, tt.n2, tt.n3, tt.n4); ret != tt.expected {
			t.Errorf("Error with %v,%v,%v,%v", tt.n1, tt.n2, tt.n3, tt.n4)
		}
	}
}
