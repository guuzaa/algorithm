package list

import (
	"testing"
)

func TestMiniGroups(t *testing.T) {
	tests := []struct {
		intervals [][]int
		expected  int
	}{
		{[][]int{{0, 1<<31 - 1}}, 1},
		{[][]int{{5, 10}, {6, 8}, {1, 5}, {2, 3}, {1, 10}}, 3},
		{[][]int{{1, 3}, {5, 6}, {8, 10}, {11, 13}}, 1},
	}

	for _, tt := range tests {
		if ret := miniGroups(tt.intervals); ret != tt.expected {
			t.Errorf("Error with %v in miniGroups", tt.intervals)
		}

		if ret := miniGroups1(tt.intervals); ret != tt.expected {
			t.Errorf("Error with %v in miniGroups1", tt.intervals)
		}
	}

}
