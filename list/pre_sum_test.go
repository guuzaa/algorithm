package list

import "testing"

func TestPreSum(t *testing.T) {

	tests := []struct {
		input               []int
		left, right, output int
	}{
		{[]int{3, 5, 2, -2, 4, 1}, 0, 2, 10},
		{[]int{3, 5, 2, -2, 4, 1}, 0, 3, 8},
		{[]int{3, 5, 2, -2, 4, 1}, 1, 3, 5},
		{[]int{3, 5, 2, -2, 4, 1}, 1, 1, 5},
		{[]int{3, 5, 2, -2, 4, 1}, 5, 5, 1},
		{[]int{3, 5, 2, -2, 4, 1}, 0, 5, 13},
	}

	for _, tt := range tests {
		sum := NewPreSum(tt.input)
		if ret := sum.SumRange(tt.left, tt.right); ret != tt.output {
			t.Errorf("Error with %v[%d:%d]", tt.input, tt.left, tt.right)
		}
	}
}
