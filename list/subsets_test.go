package list

import (
	"reflect"
	"testing"
)

func TestSubsets(t *testing.T) {
	tests := []struct {
		input  []int
		output [][]int
	}{
		{[]int{1, 2, 3}, [][]int{{}, {1}, {1, 2}, {1, 2, 3}, {1, 3}, {2}, {2, 3}, {3}}},
		{[]int{0}, [][]int{{}, {0}}},
		{[]int{}, [][]int{{}}},
	}

	for _, tt := range tests {
		if ret := subsets(tt.input); !reflect.DeepEqual(ret, tt.output) {
			t.Errorf("Error with %v, result: %v", tt.input, ret)
		}
	}
}

func TestSubsets2(t *testing.T) {
	tests := []struct {
		input, output int
	}{
		{5, 8},
		{3, 3},
		{15, 987},
		{20, 10946},
	}

	for _, tt := range tests {
		if ret := subsets2(tt.input); ret != tt.output {
			t.Errorf("Error with %d; results: %d; expected: %d", tt.input, ret, tt.output)
		}
	}
}

func TestCombine(t *testing.T) {
	tests := []struct {
		n, k   int
		output [][]int
	}{
		{4, 2, [][]int{{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4}}},
		{1, 1, [][]int{{1}}},
	}

	for _, tt := range tests {
		if ret := combine(tt.n, tt.k); !reflect.DeepEqual(ret, tt.output) {
			t.Errorf("Error with %v, result: %v", tt.n, ret)
		}
	}
}

func TestSubsetWithDup(t *testing.T) {
	tests := []struct {
		input  []int
		output [][]int
	}{
		{[]int{1, 2, 2}, [][]int{{}, {1}, {1, 2}, {1, 2, 2}, {2}, {2, 2}}},
		{[]int{0}, [][]int{{}, {0}}},
		{[]int{}, [][]int{{}}},
	}

	for _, tt := range tests {
		if ret := subsetWithDup(tt.input); !reflect.DeepEqual(ret, tt.output) {
			t.Errorf("Error with %v, result: %v", tt.input, ret)
		}
	}
}

func TestCombintaionSum(t *testing.T) {
	tests := []struct {
		input  []int
		target int
		output [][]int
	}{
		{[]int{100, 10, 20, 70, 60, 10, 50}, 80, [][]int{{10, 10, 60}, {10, 20, 50}, {10, 70}, {20, 60}}},
	}

	for _, tt := range tests {
		if ret := combinationSum(tt.input, tt.target); reflect.DeepEqual(ret, tt.output) {
			t.Errorf("Error with %v", tt.input)
		}
	}
}
