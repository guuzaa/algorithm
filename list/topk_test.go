package list

import "testing"

func TestFindKthLargest(t *testing.T) {
	tests := []struct {
		input    []int
		index    int
		expected int
	}{
		{[]int{1, 4, 3, 98, 7}, 1, 98},
		{[]int{3, 2, 3, 1, 2, 4, 5, 5, 6}, 4, 4},
		{[]int{2}, 1, 2},
	}

	for _, tt := range tests {
		ret := findKthLargest(tt.input, tt.index)
		if ret != tt.expected {
			t.Fail()
		}
	}

}
