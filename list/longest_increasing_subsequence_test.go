package list

import "testing"

func TestLengthofLISI(t *testing.T) {
	tests := []struct {
		input    []int
		expected int
	}{
		{[]int{7, 7, 7, 7, 7}, 1},
		{[]int{10, 9, 2, 5, 3, 7, 101, 18}, 4},
		{[]int{0, 1, 0, 3, 2, 3}, 4},
	}

	for i, tt := range tests {
		if ret := LengthofLISI(tt.input); ret != tt.expected {
			t.Errorf("[%d] -- Error with %v", i, tt.input)
		}

		if ret := LengthofLISI1(tt.input); ret != tt.expected {
			t.Errorf("LISI1 -- [%d] -- Error with %v", i, tt.input)
		}
	}
}
