package list

import "testing"

func TestFindDuplicates(t *testing.T) {
	tests := []struct {
		input    []int
		expected int
	}{
		{[]int{1, 3, 4, 2, 2}, 2},
		{[]int{3, 1, 3, 4, 2}, 3},
	}

	for _, tt := range tests {
		if ret := FindDuplicate(tt.input); ret != tt.expected {
			t.Errorf("Error with %v", tt.input)
		}
	}
}
