package list

// Problem Definition: https://www.nowcoder.com/practice/6fbe70f3a51d44fa9395cfc49694404f

func findMedianTwoSortedAray(n1, n2 []int) int {
	n := len(n1)
	l1, r1, l2, r2 := 0, n-1, 0, n-1

	for l1 < r1 {
		even := (r1 - l1) % 2
		mid1 := l1 + (r1-l1)/2
		mid2 := l2 + (r2-l2)/2

		switch {
		case n1[mid1] == n2[mid2]:
			return n1[mid1]
		case n1[mid1] < n2[mid2]:
			l1 = mid1 + even
			r2 = mid2
		default:
			l2 = mid2 + even
			r1 = mid1
		}
	}

	return min(n1[l1], n2[l2])
}

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}
