package list

import (
	"reflect"
	"testing"
)

func TestSortColors(t *testing.T) {
	tests := []struct {
		input    []int
		expected []int
	}{
		{[]int{2, 0, 2, 1, 1, 0}, []int{0, 0, 1, 1, 2, 2}},
		{[]int{2, 0, 1}, []int{0, 1, 2}},
		{[]int{1, 1, 1, 1}, []int{1, 1, 1, 1}},
		{[]int{2, 2, 2, 2}, []int{2, 2, 2, 2}},
	}

	for _, tt := range tests {
		if ret := sortColors(tt.input); !reflect.DeepEqual(tt.expected, ret) {
			t.Errorf("Error Results: %v, want: %v", ret, tt.expected)
		}

		if ret := sortColors1(tt.input); !reflect.DeepEqual(tt.expected, ret) {
			t.Errorf("[1]Error Results: %v, want: %v", ret, tt.expected)
		}
	}
}
