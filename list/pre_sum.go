package list

import "gitee.com/guuzaa/algorithm/constraints"

type PreSum[T constraints.Integer] struct {
	preSum []T
}

func NewPreSum[T constraints.Integer](nums []T) *PreSum[T] {
	sum := make([]T, len(nums)+1)
	for i := 1; i <= len(nums); i++ {
		sum[i] = sum[i-1] + nums[i-1]
	}

	return &PreSum[T]{sum}

}

func (p *PreSum[T]) SumRange(i, j int) T {
	return p.preSum[j+1] - p.preSum[i]
}
