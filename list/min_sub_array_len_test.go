package list

import "testing"

func TestMinSubArrayLen(t *testing.T) {
	tests := []struct {
		input  []int
		target int
		want   int
	}{
		{[]int{1, 1, 1, 1, 1, 1, 1, 1}, 11, 0},
		{[]int{1, 4, 4}, 4, 1},
		{[]int{2, 3, 1, 2, 4, 3}, 7, 2},
	}
	for _, tt := range tests {
		if ret := minSubArrayLen(tt.target, tt.input); ret != tt.want {
			t.Errorf("Error Result: %v, Want %v", ret, tt.want)
		}
	}
}
