package list

import (
	"reflect"
	"testing"
)

func TestReorderArray(t *testing.T) {
	tests := []struct {
		input []int
		want  []int
	}{
		{[]int{1, 2, 3, 4}, []int{1, 3, 2, 4}},
		{[]int{2, 4, 6, 5, 7}, []int{5, 7, 2, 4, 6}},
		{[]int{1, 3, 5, 6, 7}, []int{1, 3, 5, 7, 6}},
		{[]int{}, []int{}},
		{[]int{1}, []int{1}},
		{[]int{2}, []int{2}},
	}

	for _, tt := range tests {
		if ret := reorderArray(tt.input); !reflect.DeepEqual(tt.want, ret) {
			t.Errorf("Error results: %v, want %v", ret, tt.input)
		}

		if ret := reorderArray1(tt.input); !reflect.DeepEqual(tt.want, ret) {
			t.Errorf("[reorderArray1] Error results: %v, want %v", ret, tt.input)
		}
	}
}
