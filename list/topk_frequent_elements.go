package list

import (
	"github.com/emirpasic/gods/trees/binaryheap"
	"github.com/emirpasic/gods/utils"
)

// Problem Defintion: https://leetcode.cn/problems/top-k-frequent-elements/

func topKFrequent(nums []int, k int) []int {
	ret := make([]int, 0, k)
	hash := make(map[int]int)
	for _, num := range nums {
		hash[num]++
	}

	que := binaryheap.NewWith(func(a, b interface{}) int {
		return -utils.IntComparator(a.([2]int)[0], b.([2]int)[0])
	})

	for k, v := range hash {
		que.Push([2]int{v, k})
	}

	for i := 0; i < k; i++ {
		val, _ := que.Pop()
		ret = append(ret, val.([2]int)[1])
	}

	return ret

}
