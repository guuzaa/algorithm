package list

import "testing"

func TestSelectKnums(t *testing.T) {
	tests := []struct {
		nums      []int
		k, target int
		expected  int
	}{
		{[]int{3, 6, 5}, 2, 7, 5},
		{[]int{4, 7}, 1, 5, 7},
	}

	for _, tt := range tests {
		if ret := selectKNums(tt.nums, tt.k, tt.target); ret != tt.expected {
			t.Errorf("Error with %v", tt.nums)
		}
	}
}
