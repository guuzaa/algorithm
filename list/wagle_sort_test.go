package list

import (
	"reflect"
	"testing"
)

func TestWagleSort(t *testing.T) {
	tests := []struct {
		input, output []int
	}{
		{[]int{1, 5, 1, 1, 6, 4}, []int{1, 6, 1, 5, 1, 4}},
		{[]int{1, 3, 2, 2, 3, 1}, []int{2, 3, 1, 3, 1, 2}},
	}

	for _, tt := range tests {
		if wiggleSort(tt.input); !reflect.DeepEqual(tt.input, tt.output) {
			t.Errorf("Error with %v", tt.input)
		}
	}

}
