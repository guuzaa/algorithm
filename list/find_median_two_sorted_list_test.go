package list

import "testing"

func TestFindMedianTwoSortedLists(t *testing.T) {
	tests := []struct {
		n1, n2 []int
		mid    int
	}{
		{[]int{1, 2, 3, 4}, []int{3, 4, 5, 6}, 3},
		{[]int{0, 1, 2}, []int{3, 4, 5}, 2},
		{[]int{1}, []int{2}, 1},
	}

	for _, tt := range tests {
		if ret := findMedianTwoSortedAray(tt.n1, tt.n2); ret != tt.mid {
			t.Errorf("Error with %v, %v", tt.n1, tt.n2)
		}
	}
}
