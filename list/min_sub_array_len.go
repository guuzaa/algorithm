package list

// Problem Definition: https://leetcode.cn/problems/minimum-size-subarray-sum/

func minSubArrayLen(target int, nums []int) int {
	var left, right int
	var sum int
	ret := 1 << 31

	for right < len(nums) {
		sum += nums[right]
		for sum >= target {
			ret = min(ret, right-left+1)
			sum -= nums[left]
			left++
		}
		right++
	}

	if ret == 1<<31 {
		return 0
	}
	return ret
}
