package algorithm

import (
	"testing"
)

func TestFractionToDecimal(t *testing.T) {
	tests := []struct {
		numerator, denominator int
		want                   string
	}{
		{2, 1, "2"},
		{4, 2, "2"},
		{4, 8, "0.5"},
		{4, 333, "0.(012)"},
		{821, 370, "2.2(189)"},
	}

	for _, tt := range tests {
		if ret := fractionToDecimal(tt.numerator, tt.denominator); ret != tt.want {
			t.Errorf("Error Result: %s, want %s", ret, tt.want)
		}
	}
}
