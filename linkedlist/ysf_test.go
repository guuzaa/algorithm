package linkedlist

import "testing"

func TestYsf(t *testing.T) {
	tests := []struct {
		n, m int
		want int
	}{
		{5, 2, 3},
		{1, 1, 1},
		{1000, 1, 1000},
		{1000, 8, 185},
	}

	for _, tt := range tests {
		if ret := ysf(tt.n, tt.m); ret != tt.want {
			t.Errorf("Error Result: %v, want %v", ret, tt.want)
		}

		if ret := ysf1(tt.n, tt.m); ret != tt.want {
			t.Errorf("Error Result: %v, want %v[ysf1]", ret, tt.want)
		}

		if ret := ysf2(tt.n, tt.m); ret != tt.want {
			t.Errorf("Error Result: %v, want %v[ysf2]", ret, tt.want)
		}
	}
}
