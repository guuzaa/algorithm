package graph

import (
	"reflect"
	"testing"
)

func TestAllPathSourceTarget(t *testing.T) {
	tests := []struct {
		input, output [][]int
	}{
		{
			[][]int{{1, 2}, {3}, {3}, {}},
			[][]int{{0, 1, 3}, {0, 2, 3}},
		},
		{
			[][]int{{4, 3, 1}, {3, 2, 4}, {3}, {4}, {}},
			[][]int{{0, 4}, {0, 3, 4}, {0, 1, 3, 4}, {0, 1, 2, 3, 4}, {0, 1, 4}},
		},
	}

	for _, tt := range tests {
		if ret := allPathsSourceTarget(tt.input); !reflect.DeepEqual(tt.output, ret) {
			t.Errorf("There is something wrong with : %v", tt.input)
		}
	}

}
