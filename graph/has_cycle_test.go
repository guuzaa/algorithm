package graph

import (
	"reflect"
	"testing"
)

func TestHasCycle(t *testing.T) {
	tests := []struct {
		input    [][]int
		numNodes int
		output   bool
	}{
		{[][]int{{1, 2}, {2, 3}, {3, 4}, {4, 5}}, 5, false},
		{[][]int{{2, 1}, {1, 2}}, 2, true},
		{[][]int{{2, 1}, {3, 2}}, 3, false},
	}

	for _, tt := range tests {
		graph := buildGraph(tt.input, tt.numNodes)
		if ret := hasCycle(graph, tt.numNodes); ret != tt.output {
			t.Errorf("Error with %v", tt.input)
		}
	}
}

func TestTopologicalSorting(t *testing.T) {
	tests := []struct {
		input    [][]int
		numNodes int
		output   []int
	}{
		{[][]int{{1, 2}, {2, 3}, {3, 4}, {4, 5}}, 5, []int{1, 2, 3, 4, 5}},
	}

	for _, tt := range tests {
		graph := buildGraph(tt.input, tt.numNodes)
		if ret := topologicalSorting(graph, tt.numNodes); !reflect.DeepEqual(tt.output, ret) {
			t.Errorf("Error with %v", tt.input)
		}
	}
}

func buildGraph(nums [][]int, n int) [][]int {
	graph := make([][]int, n+1)
	for _, item := range nums {
		from, to := item[0], item[1]
		graph[from] = append(graph[from], to)
	}
	return graph
}
