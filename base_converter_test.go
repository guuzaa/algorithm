package algorithm

import (
	"strconv"
	"strings"
	"testing"
)

func TestBaseConverter(t *testing.T) {
	tests := []struct {
		input int
		base  int
	}{
		{100, 8},
		{100, 2},
		{1000, 3},
		{1000, 2},
		{1000, 8},
		{1000, 13},
		{22, 16},
		{-22, 16},
		{-222, 8},
	}

	for _, tt := range tests {
		if baseConverter(tt.input, tt.base) != strings.ToUpper(strconv.FormatInt(int64(tt.input), tt.base)) {
			t.Errorf("There is something wrong with %d %d", tt.input, tt.base)
		}
	}
}
