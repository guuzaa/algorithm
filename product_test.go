package algorithm

import "testing"

func BenchmarkProduct1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = product1(100, 100)
		_ = product1(101, 100003)
		_ = product1(1001, 1000553)
		_ = product1(89001, 1000553)
	}
}

func BenchmarkProduct2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = product2(100, 100)
		_ = product2(101, 100003)
		_ = product2(1001, 1000553)
		_ = product2(89001, 1000553)
	}
}

func BenchmarkProduct3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = product3(100, 100)
		_ = product3(101, 100003)
		_ = product3(1001, 1000553)
		_ = product3(89001, 1000553)
	}
}
