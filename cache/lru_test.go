package cache

import (
	"testing"
)

func TestLRUCache(t *testing.T) {
	capacity := 3
	cache := NewCache(capacity)

	cache.Set(1, 1)
	cache.Set(2, 2)
	cache.Set(3, 3)

	if ret := cache.Get(1); ret != 1 {
		t.Errorf("Error with Get")
	}

	if ret := cache.Get(2); ret != 2 {
		t.Errorf("Error with Get")
	}

	cache.Set(1, 10)
	if ret := cache.Get(1); ret != 10 {
		t.Errorf("Error with Set")
	}

	cache.Set(4, 4)
	if ret := cache.Get(3); ret != -1 {
		t.Errorf("Error with removeOldest")
	}

}
