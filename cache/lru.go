package cache

import "container/list"

type (
	entry struct {
		key, value int
	}

	LRUCache struct {
		ll             *list.List
		cache          map[int]*list.Element
		capacity, size int
	}
)

func NewCache(capacity int) *LRUCache {
	return &LRUCache{
		ll:       list.New(),
		cache:    make(map[int]*list.Element),
		capacity: capacity,
	}
}

func (c *LRUCache) Get(key int) int {
	if ele, ok := c.cache[key]; ok {
		c.ll.MoveToFront(ele)
		ety := ele.Value.(*entry)
		return ety.value
	}
	return -1
}

func (c *LRUCache) removeOldest() {
	if ele := c.ll.Back(); ele != nil {
		c.ll.Remove(ele)
		ety := ele.Value.(*entry)
		delete(c.cache, ety.key)
		c.size--
	}
}

func (c *LRUCache) Set(key, value int) {
	if ele, ok := c.cache[key]; ok {
		c.ll.MoveToFront(ele)
		ety := ele.Value.(*entry)
		ety.value = value
	} else {
		ele := c.ll.PushFront(&entry{key, value})
		c.cache[key] = ele
		c.size++
	}

	if c.capacity > 0 && c.size > c.capacity {
		c.removeOldest()
	}
}
