package algorithm

import "testing"

func TestIsPrime(t *testing.T) {
	tests := []struct {
		input int
		want  bool
	}{
		{361, false},
		{316, false},
		{136, false},
		{163, true},
		{613, true},
		{631, true},
		{419, true},
		{491, true},
		{941, true},
		{1, true},
		{4, false},
	}

	for _, tt := range tests {
		if ret := isPrime(tt.input); ret != tt.want {
			t.Errorf("Error result of %d, want %v", tt.input, tt.want)
		}
	}
}

func TestCountPrimes(t *testing.T) {
	tests := []struct {
		input, want int
	}{
		{0, 0},
		{10, 4},
		{1, 0},
		{4667930, 326842},
		{5000000, 348513},
	}

	for _, tt := range tests {
		if ret := countPrimes(tt.input); ret != tt.want {
			t.Errorf("Error Results: %d, want %d", ret, tt.want)
		}
	}
}
