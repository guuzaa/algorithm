package string

import "testing"

func TestReplace(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{"Hello{AC}W{DD}orld", "HelloWorld"},
		{"{ABCD}", ""},
		{"ad{XYZ}cdc{Y}f{x}e", "adcdcfe"},
	}

	for _, tt := range tests {
		if ret := replace(tt.input); ret != tt.expected {
			t.Errorf("The destination string should be %s, not %s", tt.expected, ret)
		}
	}
}

func TestReplaceHelper(t *testing.T) {
	tests := []struct {
		input    string
		dest     string
		expected string
	}{
		{"Hello{AC}W{DD}orld", "", "HelloWorld"},
		{"{ABCD}", "[a-zA-Z]*", "[a-zA-Z]*"},
		{"ad{XYZ}cdc{Y}f{x}e", "", "adcdcfe"},
		{"ad{XYZ}cdc{Y}f{x}e", "[a-zA-Z]*", "ad[a-zA-Z]*cdc[a-zA-Z]*f[a-zA-Z]*e"},
	}

	for _, tt := range tests {
		if ret := replaceHelper(tt.input, tt.dest); ret != tt.expected {
			t.Errorf("The destination string should be %s, not %s", tt.expected, ret)
		}
	}
}

func TestGrep(t *testing.T) {
	re := `\w+`
	filename := "./regexp_test.go"
	grep(re, filename)
}
