package string

import "testing"

func TestCountSubstrings(t *testing.T) {
	tests := []struct {
		input string
		want  int
	}{
		{"abc", 3},
		{"aaa", 6},
	}

	for _, tt := range tests {
		if ret := countSubstrings(tt.input); ret != tt.want {
			t.Errorf("Error Result: %v, want: %v", ret, tt.want)
		}
	}
}
