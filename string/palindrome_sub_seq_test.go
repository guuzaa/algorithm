package string

import "testing"

func TestPalidromeSubSeq(t *testing.T) {
	s1 := "ab"
	ret1 := LongestPalindromeSubSeq(s1)
	t.Log(ret1)

	s2 := "abcdewa"
	ret2 := LongestPalindromeSubSeq(s2)
	t.Log(ret2)
}

func TestPalidrome(t *testing.T) {
	s1 := "abbba"
	ret1 := LongestPalindromeSeq(s1)
	t.Log(ret1)
	if ret1 != 5 {
		t.Fail()
	}

	s2 := "ababc"
	ret2 := LongestPalindromeSeq(s2)
	t.Log(ret2)

	if ret2 != 3 {
		t.Fail()
	}
}
