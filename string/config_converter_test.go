package string

import (
	"reflect"
	"testing"
)

func TestConfigConerter(t *testing.T) {
	tests := []struct {
		input, want []string
	}{
		{[]string{
			"-A1: v1",
			"   B1: v2",
			"     C1: v3",
			"     C2: v4",
			"       D1: v5",
			"-A2: v5",
			"   B2: v6",
			"     C1: v8",
			"   B3: v7",
			"     C8: v9",
			"#",
		}, []string{
			"-A1: v1",
			"-A1-B1: v2",
			"-A1-B1-C1: v3",
			"-A1-B1-C2: v4",
			"-A1-B1-C2-D1: v5",
			"-A2: v5",
			"-A2-B2: v6",
			"-A2-B2-C1: v8",
			"-A2-B3: v7",
			"-A2-B3-C8: v9",
			"#",
		}},
		{[]string{
			"-A1: v1",
			"   B1: v2",
			"     C1: v3",
			"     C2: v4",
			"       D1: v5",
			"-A2: v5",
			"   B2: v6",
			"     C1: v8",
			"   B3: v7",
			"#",
		}, []string{
			"-A1: v1",
			"-A1-B1: v2",
			"-A1-B1-C1: v3",
			"-A1-B1-C2: v4",
			"-A1-B1-C2-D1: v5",
			"-A2: v5",
			"-A2-B2: v6",
			"-A2-B2-C1: v8",
			"-A2-B3: v7",
			"#",
		}},
		{[]string{
			"-A1: v1",
			"   B1: v2",
			"     C1: v3",
			"     C2: v4",
			"       D1: v5",
			"         E1: v6",
			"           F1: v7",
			"#",
		}, []string{
			"-A1: v1",
			"-A1-B1: v2",
			"-A1-B1-C1: v3",
			"-A1-B1-C2: v4",
			"-A1-B1-C2-D1: v5",
			"-A1-B1-C2-D1-E1: v6",
			"-A1-B1-C2-D1-E1-F1: v7",
			"#",
		}},
	}

	for _, tt := range tests {
		if ret := Converter(tt.input); !reflect.DeepEqual(ret, tt.want) {
			t.Errorf("Error with %v, get %v", tt.input, ret)
		}
	}

}
