package string

import "testing"

func TestPartition(t *testing.T) {
	s1 := "aab"
	ret1 := Partition(s1)
	t.Log(ret1)

	s2 := "a"
	ret2 := Partition(s2)
	t.Log(ret2)
}
