package string

// Problem Definition: https://leetcode.cn/problems/palindromic-substrings/

func countSubstrings(s string) (ret int) {
	n := len(s)
	for i := 0; i < 2*n-1; i++ {
		left, right := i/2, i/2+i%2
		for left >= 0 && right < n && s[left] == s[right] {
			left--
			right++
			ret++
		}
	}

	return
}
