package string

import (
	"testing"
)

func TestLogestSubString(t *testing.T) {
	tests := []struct {
		input  string
		output int
	}{
		{"bbbb", 1},
		{"abcabcbb", 3},
		{"pwwkew", 3},
	}

	for _, tt := range tests {
		if ret := lengthOfLongestSubstring(tt.input); ret != tt.output {
			t.Errorf("Error with %s", tt.input)
		}
	}

}
