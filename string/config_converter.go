package string

import (
	"strings"
)

/*
Copy from Media interview

-A1: v1
    B1: v2
        C1: v3
        C2: v4
-A2: v5
     B2: v6
     B3: v7
#
== >
-A1: v1
-A1-B1: v2
-A1-B1-C1: v3
-A1-B1-C2: v4
-A2: v5
-A2-B2: v6
-A2-B3: v7
#
*/

func Converter(lst []string) []string {
	n := len(lst)
	tags := make([]string, 26)
	ret := make([]string, 0, n)
	for i := 0; i < n-1; i++ {
		idx := strings.Index(lst[i], ":")
		cur := idx / 2
		if cur == 1 {
			tags[cur-1] = lst[i][:idx]
		} else {
			tags[cur-1] = lst[i][idx-2 : idx]
		}

		if cur > 1 {
			front := strings.Join(tags[:cur-1], "-")
			end := "-" + lst[i][idx-2:]
			ret = append(ret, front+end)
		} else {
			ret = append(ret, lst[i])
		}
	}

	ret = append(ret, "#")

	return ret
}
