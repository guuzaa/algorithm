package string

// Problem Definition: https://leetcode.cn/problems/longest-substring-without-repeating-characters/

func lengthOfLongestSubstring(s string) int {
	hash := make(map[byte]int)
	var ret int

	left, right := 0, 0
	for right < len(s) {
		b := s[right]
		hash[b]++
		right += 1

		for hash[b] > 1 {
			hash[s[left]]--
			left++
		}
		ret = max(ret, right-left)
	}

	return ret
}
