package string

import "testing"

func TestIsMatch(t *testing.T) {
	tests := []struct {
		s, p     string
		expected bool
	}{
		{"", "h", false},
		{"", "?", false},
		{"", "*", true},
		{"ab", "*", true},
		{"ab", "?*", true},
		{"ab##", "?*", true},
	}

	for _, tt := range tests {
		if isMatch(tt.s, tt.p) != tt.expected {
			t.Errorf("There is a problem with (%s, %s)", tt.s, tt.p)
		}
	}
}

func TestIsMatch2(t *testing.T) {
	tests := []struct {
		s, p     string
		expected bool
	}{
		{"aa", "a", false},
		{"aa", "a.", true},
		{"aa", ".a", true},
		{"aa", "a*", true},
		{"aaaaaaa", "a*", true},
		{"aa", "a*aa", true},
		{"ab", ".*", true},
		{"aa", ".*", true},
	}

	for _, tt := range tests {
		if isMatch1(tt.s, tt.p) != tt.expected {
			t.Errorf("There is a problem with (%s, %s)", tt.s, tt.p)
		}
	}
}
