package heap

import "testing"

func TestMinHeap(t *testing.T) {
	heap := NewMinHeap[int]()

	heap.Push(2)
	heap.Push(-2)
	heap.Push(12)
	heap.Push(23)
	heap.Push(28)

	if ret := heap.Pop(); ret != -2 {
		t.Errorf("Top item on the queue should be -2, instead of %d", ret)
	}

	if ret := heap.Pop(); ret != 2 {
		t.Errorf("Top item on the queue should be 2, instead of %d", ret)
	}

	if ret := heap.Pop(); ret != 12 {
		t.Errorf("Top item on the queue should be 12, instead of %d", ret)
	}

	heap.Push(-2)

	if ret := heap.Pop(); ret != -2 {
		t.Errorf("Top item on the queue should be -2, instead of %d", ret)
	}

	if ret := heap.Pop(); ret != 23 {
		t.Errorf("Top item on the queue should be 23, instead of %d", ret)
	}

	if ret := heap.Pop(); ret != 28 {
		t.Errorf("Top item on the queue should be 28, instead of %d", ret)
	}
}
