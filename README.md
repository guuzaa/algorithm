# Go 算法库

### 回溯算法
- [子集](./list/subsets.go)
- [排列](./list/permutation.go)
- [数独](./list/sudoku.go)


### BFS 广度搜索算法
- [完美平方数](./num_squares.go)


### 动态规划
- [完美平方数](./num_squares.go)
- [最长递增子序列](./list/longest_increasing_subsequence.go)
- [购物单](./list/shopping_list.go)


### 模拟算法
- [进制转换](./base_converter.go)
- [LRU 最近最少使用](./cache/lru.go)
- [分数转小数](./fraction_to_decimal.go)
- [计算素数/质数的个数](./is_prime.go#L12)
- [解数独](./list/sudoku.go)


### 列表
- [Top K](./list/topk.go)
- [右侧更小值数目](./list/count_smaller.go)
- [数组最长连续子序列](./list/max_increasing_seq.go)
- [数据流的中位数](./list/median_finder.go)
- [滑动窗口最大值](./list/max_sliding_window.go)
- [搜索二维有序矩阵](./list/search_matrix.go)
- [有序矩阵第 k 小的元素](./list/search_matrix.go)  [二分]
- [四数相加](./list/four_nums_sum.go#L3)
- [加起来和为目标值的组合](./list/subsets.go#L110)
- [前缀和](./list/pre_sum.go)
- [摆动排序](./list/wagle_sort.go)
- [四数之和](./list/four_nums_sum.go)
- [前 k 个高频元素](./list/topk_frequent_elements.go)
- [选数](./list/select_k_nums.go)
- [天际线](./list/get_skyline.go)
- [将区间分成最少组数](./list/mini_groups.go)
- [选数](./list/select_k_nums.go)  [二分]
- [最长递增子序列](./list/longest_increasing_subsequence.go)
- [最大正方形](./list/max_square.go)
- [重排数组](./list/reorder_array.go)
- [颜色分类（荷兰国旗问题）](./list/sort_colors.go)
- [长度最小子数组](./list/min_sub_array_len.go)


### 栈
- [栈](./stack/stack.go)
- [最小栈](./stack/min_stack.go)


### 堆
- [小顶堆](./heap/min_heap.go)
- [数据流的中位数](./list/median_finder.go)


### 树
- [二叉搜索树](./tree/bstree/bstree.go)
- [AVL 树](./tree/avl/avl.go)
- [前缀树](./tree/trie.go)


### 图
- [遍历](./graph/traverse.go)
- [判断是否存在环](./graph/has_cycle.go)
- [拓扑排序](./graph/has_cycle.go)


### 字符串处理
- [最长回文子串](./string/palindrome_sub_seq.go)
- [回文子串数目](./string/palindromic_substrings.go)
- [最长回文子序列](./string/palindrome_sub_seq.go)
- [单词搜索](./string/find_word.go)
- [正则表达式处理](./string/regexp.go)
- [模糊匹配](./string/asterisk_match.go)
- [最长无重复子串](./string/longest_sub_string.go)