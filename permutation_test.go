package algorithm

import (
	"reflect"
	"testing"
)

func TestPermutation(t *testing.T) {
	tests := []struct {
		input int
		want  []int
	}{
		{123, []int{132, 213, 231, 312, 321}},
		{21, []int{12}},
		{1010, []int{1001, 1100}},
	}

	for _, tt := range tests {
		if ret := permutation(tt.input); !reflect.DeepEqual(ret, tt.want) {
			t.Errorf("Error Results: %v\n\t want %v", ret, tt.want)
		}
	}
}
