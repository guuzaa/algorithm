package algorithm

import "testing"

func TestNumSquare(t *testing.T) {
	tests := []struct {
		input, output int
	}{
		{12, 3},
		{13, 2},
		{1000, 2},
		{999, 4},
		{666, 2},
		{4, 1},
		{10000, 1},
	}

	t.Run("Test numSquares1", func(t *testing.T) {
		t.Parallel()
		for _, tt := range tests {
			if ret := numSquares1(tt.input); ret != tt.output {
				t.Errorf("Error with %d, the wrong result is %d", tt.input, ret)
			}
		}
	})

	t.Run("Test numSquares2", func(t *testing.T) {
		t.Parallel()

		for _, tt := range tests {
			if ret := numSquares2(tt.input); ret != tt.output {
				t.Errorf("Error with %d, the wrong result is %d", tt.input, ret)
			}
		}
	})
}
