package algorithm

func product1(a, b int) int {
	return a * b
}

func product2(a, b int) int {
	ret := 1
	for i := 0; i < b; i++ {
		ret *= a
	}

	return ret
}

func product3(a, b int) int {
	if b == 0 {
		return 0
	}
	return a + product3(a, b-1)
}
