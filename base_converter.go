package algorithm

import (
	"strings"

	"gitee.com/guuzaa/algorithm/stack"
)

func baseConverter(num int, base int) string {
	digits := []byte{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'}

	s := stack.New()

	var isNegative bool
	if num < 0 {
		isNegative = true
		num = -num
	}

	for num > 0 {
		s.Push(num % base)
		num /= base
	}

	var ret strings.Builder

	if isNegative {
		ret.WriteByte('-')
	}

	for !s.IsEmpty() {
		rem := s.Pop().(int)
		ret.WriteByte(digits[rem])
	}

	return ret.String()
}
