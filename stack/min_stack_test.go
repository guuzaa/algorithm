package stack

import "testing"

func TestMinStack(t *testing.T) {
	stack := NewMinStack[int]()

	stack.Push(-2)
	if stack.GetMin() != -2 {
		t.Errorf("Min value on the stack should be -2")
	}

	if stack.Peek() != -2 {
		t.Errorf("Top item on the stack should be -2")
	}

	stack.Push(0)
	stack.Push(-3)

	if stack.GetMin() != -3 {
		t.Errorf("Min value on the stack should be -3")
	}

	stack.Pop()

	if stack.Peek() != 0 {
		t.Errorf("Top item on the stack should be 0")
	}

	if stack.GetMin() != -2 {
		t.Errorf("Min value on the stack should be -2")
	}
}
